const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("FakeToken", function () {
  let token;
  let owner;

  beforeEach(async function () {
    const FakeToken = await ethers.getContractFactory("FakeToken");
    token = await FakeToken.deploy("MyToken", "MTK");
    await token.deployed();
    owner = await ethers.provider.getSigner(0);
    // Mint some tokens to the owner for testing purposes
    // await token._mint(owner.getAddress(), ethers.utils.parseEther("100"));
  });

  it("should transfer tokens", async function () {
    const to = ethers.Wallet.createRandom().address;
    const amount = ethers.utils.parseEther("10");
    // Make sure the owner has enough balance before transferring
    expect(await token.balances(owner.getAddress())).to.be.at.least(amount);
    await token.connect(owner).transfer(to, amount);
    expect(await token.balances(owner.getAddress())).to.equal(
      ethers.utils.parseEther("90")
    );
    expect(await token.balances(to)).to.equal(amount);
  });
  it("should approve and transferFrom tokens", async function () {
    
    const to = ethers.provider.getSigner(1).getAddress();

    const amount = ethers.utils.parseEther("10");
    // Approve the transfer from owner to to
    await token.connect(owner).approve(to, amount);
    // Make sure the owner has enough balance before transferring
    expect(await token.balances(owner.getAddress())).to.be.at.least(amount);
    // // Transfer tokens from owner to to using transferFrom
    await token
      .connect(ethers.provider.getSigner(1))
      .transferFrom(owner.getAddress(), to, amount);
    expect(await token.balances(owner.getAddress())).to.equal(
      ethers.utils.parseEther("90")
    );
    expect(await token.balances(to)).to.equal(amount);
  });
});
